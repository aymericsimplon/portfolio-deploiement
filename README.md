    Ce projet est un site web portfolio pour présenter un peu ce que je fais

  Ma page est assez simple pour le moment je ferai mieux plus tard dans l'année mais pour l'instant c'est en grande partie grâce à boostrap et ses classes que j'ai pu réaliser ce portfolio

    En-tête : Il contient le menu de navigation
    Première section: Courte présentation ainsi que mes futurs objectifs
    Deuxième section: Avec une boite basculante permet de montrer les langages maitrisés et qu'on verra au fil de l'année, ( dans les années on veut tout maitriser comme avatar le maitre de l'air tout les éléments)
    Projets : Présente quelques projets sur lesquels on a travaillé, rien de fou pour le moment
    Contact : Affiche les liens sociaux dans le pied de page pour GitLab, Email et LinkedIn.
    Pied de Page : Inclut des informations sur les droits d'auteur et les crédits.

    Fonctionnalités

    Design adaptatif utilisant Bootstrap pour une expérience fluide sur tous les appareils.
    Design de boîte basculante pour mettre en avant les compétences en langages de programmation.
    Section Projets avec images, titres, descriptions et liens.
    Liens sociaux dans le pied de page pour GitLab, Email et LinkedIn.

    lien maquette figma: https://www.figma.com/file/9japzy9Cgzge72xETjDkRr/Untitled?type=design&node-id=0%3A1&mode=design&t=p3rYGWu4UCIwO6Ov-1git